
# Données du Psautier de Sainte Lucie

Les données sont stockées dans des fichiers CSV

Le séparateur est le point-virgule

## Outil d'édition

LibreOffice

Configuration :

- A l'ouverture du fichier :
* Character set : Unicode (UTF-8)
* Language : French (France)
* Separator options : Separated by Semicolon
![Configuration de LibreOffice à l'ouverture du CSV](libreoffice_settings.png)

- Pendant la saisie de données :
* Attention à la majuscule automatique en début de cellule
* Booléens "true" et "false" en minuscules : Format > Cells > Category = Text
* Date au format YYYY-MM-DD (ex : 2022-02-22)

## Calendrier liturgique

Les colonnes sont les suivantes : Date;Célébrations (à titre indicatif);Est un dimanche;Année liturgique;Fichiers

- Date : format YYYY-MM-DD
- Célébrations (à titre indicatif)
- Est un dimanche ou une fête : booléen en minuscule ("true" ou "false")
- Année liturgique : A, B ou C
- Fichiers : liste des codes des fichiers de psaumes. Séparateur virgule

## Psaumes

Les colonnes sont les suivantes : Fichier;Cérémonie;N° psaume;Versets;Antienne;Année liturgique;Version

- Fichier : code du fichier de psaume
- Cérémonie : libellé de la cérémonie concernée. C'est ce libellé qui sera affiché dans le détail d'un psaume
- N° psaume : numéro du psaume, ex : Ps 49
- Versets :
- Antienne
- Année liturgique : A, B, C ou ABC
- Version : vide ou 2

## Autres compositions

Les colonnes sont les suivantes : Fichier;Titre;Nombre de pages;A un fichier audio de Polyphonie;A un fichier audio de Soprano;A un fichier audio d’Alto;A un fichier audio de Ténor;A un fichier audio de Basse

- Fichier : code du fichier de la composition
- Titre
- Nombre de pages
- A un fichier audio de Polyphonie : booléen en minuscule ("true" ou "false")
- A un fichier audio de Soprano : booléen en minuscule ("true" ou "false")
- A un fichier audio d’Alto : booléen en minuscule ("true" ou "false")
- A un fichier audio de Ténor : booléen en minuscule ("true" ou "false")
- A un fichier audio de Basse : booléen en minuscule ("true" ou "false")

## Transcriptions pour orgue

Les colonnes sont les suivantes : Fichier;Titre;Compositeur;Nombre de pages;A un fichier audio de Polyphonie;A un fichier audio de Soprano;A un fichier audio d’Alto;A un fichier audio de Ténor;A un fichier audio de Basse

- Fichier : code du fichier de la transcription
- Titre
- Compositeur
- Nombre de pages
- A un fichier audio de Polyphonie : booléen en minuscule ("true" ou "false")
- A un fichier audio de Soprano : booléen en minuscule ("true" ou "false")
- A un fichier audio d’Alto : booléen en minuscule ("true" ou "false")
- A un fichier audio de Ténor : booléen en minuscule ("true" ou "false")
- A un fichier audio de Basse : booléen en minuscule ("true" ou "false")

## Comment contribuer

### Mettre à jour son environnement

```
git checkout main

git pull origin main
```

### Faire les modifications voulues

Ajouter, modifier, supprimer des fichiers

- Les fichiers des "Psaumes" doivent se trouver dans les dossiers suivants : psaumes-pdf-mscz, psaumes-png, psaumes-audio
- Les fichiers des "Autres compositions" doivent se trouver dans les dossiers suivants : autres-pdf-mscz, autres-png, autres-audio
- Les fichiers des "Transcriptions pour orgue" doivent se trouver dans les dossiers suivants : transcriptions-pdf-mscz, transcriptions-png, transcriptions-audio

### Enregistrer son travail

```
git add -A

git commit -m "ton message"

git push origin main
```
